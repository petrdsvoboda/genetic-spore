# Evolution

Trying to improve upon Spore's first stage with genetic algorithm.

## Summary

The whole idea behind this game is to use genetic principles to this game's units. You are a cell, which can be upgraded with DNA points. You have multiple abilities and upgrades available to you. Some upgrades require energy, some generate it. You have to find the balance and choose the right upgrades. On the other hand enemies are constantly being born and dying. The enemies are generated partly with random attributes and partly based on the most successful units in the game. So the difficulty of the game is partly up to luck.

There are three global components.

### Collision resolver

Manages all game units (BaseUnit attribute). Checks collisions between them. Each GameUnit registers itself to the resolver.

There are static units (DNA, food) and dynamic units (enemies, player). Collisions are checked between static and dynamic objects and between dynamic objects.

On collision there are collision messages sent with data of units in collisions. Those collision are solved mainly by UnitBehaviour components.

### UIController

Game UI is done using HTML. UIController checks the state of Player attribute and updates the HTML. There are buttons managing user clicks. After clicking a button, messages are sent to PlayerUpgradeManager.

### ECSA.KeyInputComponent

Manages player input and is used by PlayerBehaviour.

### Game

The game object is used to hold all other objects. Those objects have then position relative to the game object. This allows to have game be used as camera (using CameraController). Game checks the position and rotation of player object and updates its values so the player is always facing top and is in the middle of the screen.

Upon player's death and restarting game, it rebuilds the game.

### Units

Units hold their information in Unit attributes. BaseUnit has just info about position, rotation, etc. EvolvableUnit holds info about abilities and upgrades. Enemy unit has some more information regarding the enemy AI.

#### Food and DNA

Food is randomly spawned on the map, it can be used to increase unit's energy.

DNA is spawned upon enemy's death, on his position. The better the enemy, the more DNA dropped.

#### Enemies

Enemies decide their actions using state machine. They also have random attributes which prefer some actions more. Those attributes are evolving using the genetic mechanisms.

For example if they are more curious, the will wander even with less energy than others. Aggressive ones will chase you, etc.

Enemies will flee when they see stronger enemies.

#### Movement

Units use very simple steering behaviors to simulate flowing in liquid. There is only forward force and rotational force.

### Spawners

Spawners are used to create new game units. There are two type of spawners. RandomSpawner crates units on random positions on the map with some rate per second. MessageSpawner spawns units on received messages.

EnemySpawner extends RandomSpawner and creates enemies. It holds information about current population. It has max population size. Upon decrease in population, it holds tournament of random selection of enemies and selects the best one. It can also crossbreed enemies. Then it creates the new mutated enemy.

### UpgradeManager

Manages units upgrades. The EnemyUpgradeManager on top of that allows enemies to randomly improve themselves. When enemies pick up new DNA, they can decide to save it or upgrade themselves. Upgrades are picked on random.

### CellGraphicsManager

Creates and updates Graphics objects of units. It is subscribed to evolve messages and when unit is evolved it updates its graphics.

## Diagram

![alt text](diagram.png "Logo Title Text 1")

## Tutorial

Left/right arrow to rotate. Up arrow to move.

Open upgrade menu with plus button on the top right. There are tooltips explaining abilities and upgrades on each item name.

The goal is to become the best cell in the Petri dish.

Don't let your energy fall below 0 or you will die.

Use DNA to buy upgrades for your cell. Be careful, they cost energy per sec! Moving also costs energy, the more you move, the more you spend.

Cheat key to increase DNA: X
