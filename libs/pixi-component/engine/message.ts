import Component from "./component"
import { GameObject } from "./game-object"

/**
 * Message that stores type of action, a relevant component, a relevant game object and custom data if needed
 */
export class BaseMessage<T extends string, U extends any = {}> {
	/**
	 * Data payload
	 */
	data: U = null

	/*
	 * If any handler sets this flag to true, the message will no longer be handled
	 */
	expired: boolean = false

	/**
	 * Action type identifier
	 */
	private _action: T = null

	/**
	 * Component that sent this message
	 */
	private _component: Component = null

	/**
	 * GameObject attached to this message
	 */
	private _gameObject: GameObject = null

	constructor(
		action: T,
		component: Component,
		gameObject: GameObject,
		data: U = null
	) {
		this._action = action
		this._component = component
		this._gameObject = gameObject
		this.data = data
	}

	get action() {
		return this._action
	}

	get component() {
		return this._component
	}

	get gameObject() {
		return this._gameObject
	}
}

export default class Message extends BaseMessage<string> {}
