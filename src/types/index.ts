import { BaseMessage } from "../../libs/pixi-component/engine/message"
import { BaseComponent } from "../../libs/pixi-component/engine/component"

import EvolvableUnit from "../atributes/EvolvableUnit"

// Message

export const MessageType = [
	"ate",
	"evolve",
	"increase",
	"decrease",
	"increaseUpgrade",
	"decreaseUpgrade",
	"unlock",
	"death",
	"game_over",
	"game_start"
] as const
export type MessageType = typeof MessageType[number]

type MessageData = {
	id: number
	type: AbilityType | UpgradeType
	other: EvolvableUnit
	upgradeId: number
}

export type Message = BaseMessage<MessageType, MessageData>
export class Component extends BaseComponent<MessageType, MessageData> {}

// Unit

export const UnitType = ["player", "enemy", "food"] as const
export type UnitType = typeof UnitType[number]

export const AbilityType = [
	"size",
	"speed",
	"agility",
	"strength",
	"defense",
	"sight"
] as const
export type AbilityType = typeof AbilityType[number]

export const UpgradeType = [
	"mitochondrion",
	"storage",
	"boost",
	"poison",
	"camouflage",
	"mutator"
] as const
export type UpgradeType = typeof UpgradeType[number]

export const CellType = [...AbilityType, ...UpgradeType]
export type CellType = AbilityType | UpgradeType
