import * as ECSA from "../libs/pixi-component"

import * as Components from "./components"
import * as Builders from "./utils/builders"
import * as Graphics from "./utils/graphics"

// TODO rename your game
class MyGame {
	engine: ECSA.GameLoop

	constructor() {
		this.engine = new ECSA.GameLoop()
		let canvas = document.getElementById("gameCanvas") as HTMLCanvasElement

		// init the game loop
		this.engine.init(
			canvas,
			800,
			600,
			1, // width, height, resolution
			{
				flagsSearchEnabled: false, // searching by flags feature
				statesSearchEnabled: false, // searching by states feature
				tagsSearchEnabled: true, // searching by tags feature
				namesSearchEnabled: true, // searching by names feature
				notifyAttributeChanges: false, // will send message if attributes change
				notifyStateChanges: false, // will send message if states change
				notifyFlagChanges: false, // will send message if flags change
				notifyTagChanges: false, // will send message if tags change
				debugEnabled: false // debugging window
			},
			true
		) // resize to screen

		this.engine.app.loader.reset().load(() => this.onAssetsLoaded())
	}

	onAssetsLoaded() {
		// init the scene and run your game
		let scene = this.engine.scene
		scene.addGlobalComponent(new Components.CollisionResolver(), true)
		scene.addGlobalComponent(new ECSA.KeyInputComponent(), true)

		new ECSA.Builder(scene)
			.globalPos(this.engine.width / 2, this.engine.height / 2)
			.anchor(0)
			.withParent(scene.stage)
			.withComponent(new Components.CameraController())
			.withComponent(new Components.GameController())
			.build()

		scene.addGlobalComponent(new Components.UIController(), true)
	}
}

// this will create a new instance as soon as this file is loaded
export default new MyGame()
