import * as ECSA from "../../libs/pixi-component"

class Map {
	center: ECSA.Vector
	size: number

	constructor(size: number = 750) {
		this.size = size
		this.center = new ECSA.Vector(this.size, this.size)
	}
}

export default Map
