import * as ECSA from "../../libs/pixi-component"
import { UnitType } from "../types"
import { getId } from "../utils/uid"

export type BaseUnitData = {
	type: UnitType
	position: ECSA.Vector
	rotation?: number
	size?: number
}

// Tracks unit position, rotation, size, ...
// Copies those units to pixiObject
class BaseUnit implements BaseUnitData {
	public id: number = getId()
	public pixiObject: PIXI.Container

	public type: UnitType
	public scaledSize: number
	private _size: number
	private _position: ECSA.Vector
	private _rotation: number = 0

	get position() {
		return this._position
	}
	set position(vector: ECSA.Vector) {
		this._position = vector

		if (this.pixiObject) {
			this.pixiObject.position.x = this._position.x
			this.pixiObject.position.y = this._position.y
		}
	}

	get rotation() {
		return this._rotation
	}
	set rotation(value: number) {
		this._rotation = value

		if (this.pixiObject) {
			this.pixiObject.rotation = this._rotation
		}
	}

	get size() {
		return this._size
	}
	set size(value: number) {
		this._size = value

		if (this.pixiObject) {
			this.pixiObject.pivot.set(this.scaledSize)
		}
	}

	constructor(data: BaseUnitData) {
		this.type = data.type
		this.position = data.position
		this.rotation = data.rotation ?? 0
		this.size = data.size ?? 1
	}
}

export default BaseUnit
