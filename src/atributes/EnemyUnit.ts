import EvolvableUnit, { UpgradeMap, UnitConfig } from "./EvolvableUnit"
import { BaseUnitData } from "./BaseUnit"

export type EnemyUnitData = {
	dna: number
	aggressivity: number
	curiosity: number
	courage: number
	intelligence: number
	greediness: number
} & UnitConfig

class EnemyUnit extends EvolvableUnit {
	public aggressivity: number = 0.1
	public curiosity: number = 0.1
	public courage: number = 0.1
	public intelligence: number = 0.1
	public greediness: number = 0.1

	public timeAlive: number = 0

	constructor(data: EnemyUnitData & BaseUnitData) {
		super(data)
		Object.assign(this, data)
	}
}

export default EnemyUnit
