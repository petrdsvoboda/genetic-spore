import { AbilityType, UpgradeType } from "../types"
import BaseUnit from "./BaseUnit"
import { applyUpgrade } from "../components/UpgradeManager"

export type UnitConfig = Record<AbilityType, number>
export type UpgradeMap = Record<UpgradeType, Record<number, number>>

class EvolvableUnit extends BaseUnit implements UnitConfig {
	public strength: number = 1
	public speed: number = 1
	public agility: number = 1
	public defense: number = 1
	public sight: number = 1

	public energy: number = 100
	public energyRegen: number = 0

	public _dna: number = 3
	public dnaRegen: number = 0

	public upgradeSlots: number = 0

	public upgrades: UpgradeMap = UpgradeType.reduce(
		(acc, curr) => ({
			...acc,
			[curr]: []
		}),
		{} as any
	)

	public upgradeId: number = 1

	get scaledSize() {
		return (this.size + 1) * 8 * 1.5
	}

	get sightRadius() {
		return (this.sight + 1) * 35
	}

	get applicableStrength() {
		return this.strength * applyUpgrade(this.upgrades, "poison")
	}

	get perceivedStrength() {
		return (
			this.applicableStrength * applyUpgrade(this.upgrades, "camouflage")
		)
	}

	get energyMax() {
		return 100 * applyUpgrade(this.upgrades, "storage")
	}

	get dna() {
		return this._dna
	}

	set dna(value: number) {
		this._dna = value
	}

	get totalDNA() {
		return (
			AbilityType.reduce((acc, curr) => acc + this[curr], 0) +
			UpgradeType.reduce(
				(acc, curr) =>
					acc +
					Object.values(this.upgrades[curr]).reduce(
						(a, c) => a + c,
						0
					),
				0
			)
		)
	}

	public addUpgrade(type: UpgradeType): number {
		const id = this.upgradeId++
		this.upgrades[type][id] = 1
		return id
	}

	public removeUpgrade(upgradeId: number, type: UpgradeType) {
		delete this.upgrades[type][upgradeId]
	}
}

export default EvolvableUnit
