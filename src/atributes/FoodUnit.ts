import BaseUnit, { BaseUnitData } from "./BaseUnit"

type FoodType = "food" | "dna"

type FoodUnitData = { scaledSize: number; value?: number } & BaseUnitData

class FoodUnit extends BaseUnit {
	public foodType: "food" | "dna"

	public scaledSize: number
	public value: number

	constructor(foodType: FoodType, data: FoodUnitData) {
		super(data)
		this.scaledSize = data.scaledSize
		this.value = data.value
		this.foodType = foodType
	}
}

export default FoodUnit
