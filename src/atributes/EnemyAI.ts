import EnemyUnit from "./EnemyUnit"
import BaseUnit from "./BaseUnit"
import EvolvableUnit from "./EvolvableUnit"
import { getAngleDifference } from "../utils/functions"

type Context = {
	unit: EnemyUnit
	target: BaseUnit | null
}

export const State = [
	"idle",
	"wandering",
	"chasing",
	"rotatingTo",
	"rotatingFrom",
	"fleeing"
] as const
export type State = typeof State[number]
export const Event = [
	"wander",
	"chase",
	"caught",
	"evaded",
	"foundTarget",
	"lostTarget",
	"saveEnergy",
	"alert"
] as const
export type Event = typeof Event[number]
type EventData = { target?: BaseUnit }
type EventObject = { type: Event; data?: EventData }

const Action = ["assignTarget", "removeTarget"] as const
type Action = typeof Action[number]
const Guard = [
	"canChase",
	"canWander",
	"saveEnergy",
	"decideChase",
	"isAway",
	"isFacing"
] as const
type Guard = typeof Guard[number]

type ActionFn = (data: EventData) => Partial<Context>
const actions: Record<Action, ActionFn> = {
	assignTarget: ({ target }) => ({ target }),
	removeTarget: () => ({ target: null })
}
type GuardFn = (context: Context, state: State) => boolean
const guards: Record<Guard, GuardFn> = {
	canChase: ({ unit }) => {
		// More courageous units will chase with less energy
		return unit.energy / unit.energyMax > 1 - unit.courage
	},
	canWander: ({ unit }) => {
		// More curious units will wander with less energy
		return unit.energy / unit.energyMax > 1 - unit.curiosity
	},
	saveEnergy: ({ unit }, state) => {
		let threshold = 0
		if (state === "wandering") {
			threshold = 1 - unit.curiosity
		} else if (state === "chasing") {
			threshold = 1 - unit.courage
		}

		// Units will save energy if it gets too low
		return unit.energy / unit.energyMax < threshold * 0.5
	},
	decideChase: ({ unit, target }) => {
		// If target is enemy or player, unit can chase
		// More probable when, it has stored energy or it is aggressive
		// Compares strengths with applied modifiers
		// Camouflage can make target more scary
		if (target instanceof EnemyUnit || target instanceof EvolvableUnit) {
			const storedEnergy = unit.energy / unit.energyMax
			const cockiness =
				storedEnergy * unit.aggressivity * unit.applicableStrength
			return cockiness + unit.strength > target.perceivedStrength * 2
		} else {
			return false
		}
	},
	isAway: ({ unit, target }) => {
		// If target can't be seen, stop running away
		if (target === null) return true
		return unit.position.distance(target.position) > unit.sightRadius
	},
	isFacing: ({ unit, target }, state) => {
		const angleDifference = getAngleDifference(
			unit,
			target,
			state === "rotatingFrom"
		)
		// Check angle facing target
		// The most intelligent units will trigger only when
		// the normalized angle is less than have difference of 0.1
		// angle 0 - Math.PI * 2, normalized 0 - 1
		const normAngle = Math.abs(angleDifference / (Math.PI * 2) - 0.5) + 0.5
		return normAngle > unit.intelligence * 0.9
	}
}

type StateData = {
	target: State
	cond?: Guard
	actions?: Action
}
type Machine = {
	initial: State
	states: Record<State, Partial<Record<Event | "", StateData | StateData[]>>>
	on: Partial<Record<Event, StateData | StateData[]>>
}

const machine: Machine = {
	initial: "idle",
	states: {
		idle: {
			"": { target: "wandering", cond: "canWander" },
			foundTarget: {
				target: "rotatingTo",
				cond: "canChase",
				actions: "assignTarget"
			}
		},
		wandering: {
			"": { target: "idle", cond: "saveEnergy" },
			foundTarget: { target: "rotatingTo", actions: "assignTarget" }
		},
		rotatingTo: {
			"": [
				{ target: "chasing", cond: "isFacing" },
				{ target: "idle", cond: "saveEnergy", actions: "removeTarget" }
			]
		},
		chasing: {
			"": { target: "idle", cond: "saveEnergy", actions: "removeTarget" }
		},
		rotatingFrom: {
			"": [
				{ target: "fleeing", cond: "isFacing" },
				{ target: "idle", cond: "isAway", actions: "removeTarget" }
			]
		},
		fleeing: {
			"": { target: "idle", cond: "isAway", actions: "removeTarget" }
		}
	},
	on: {
		alert: [
			{
				target: "rotatingTo",
				cond: "decideChase",
				actions: "assignTarget"
			},
			{ target: "rotatingFrom", actions: "assignTarget" }
		],
		lostTarget: { target: "idle", actions: "removeTarget" }
	}
}

class EnemyAI {
	private machine: Machine
	private currentState: State

	get state() {
		return this.currentState
	}

	get target() {
		return this.context.target
	}

	constructor(private context: Context) {
		this.machine = machine
		this.currentState = machine.initial
		this.send()
	}

	public send(event: Event | "" = "", data?: EventData) {
		// Find performable transitions
		let perform = this.machine.states[this.currentState][event]
		if (perform === undefined) {
			perform = this.machine.on[event]
		}
		if (perform === undefined) return

		let performTransitions: StateData[]
		if (Array.isArray(perform)) {
			performTransitions = perform
		} else {
			performTransitions = [perform]
		}

		// Get first transition that passes guard
		const transition = performTransitions.reduce(
			(acc, curr) =>
				acc !== null
					? acc
					: curr.cond === undefined
					? curr
					: guards[curr.cond](this.context, this.currentState) ===
					  true
					? curr
					: null,
			null
		)
		if (transition === null) return

		// Update context usinf actions
		if (transition.actions) {
			this.context = Object.assign(
				this.context,
				actions[transition.actions](data)
			)
		}

		// Successfull transition
		this.currentState = transition.target
	}
}

export default EnemyAI
