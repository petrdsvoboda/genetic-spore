import { Vector } from "../../libs/pixi-component"
import BaseUnit from "./BaseUnit"
import { normalizeRotation, getAngleDifference } from "../utils/functions"

const ACCELERATION_FORCE = 0.03
const ROTATION_FORCE = 0.01

export default class Dynamics {
	public aceleration: Vector = new Vector(0, 0)
	public velocity: Vector = new Vector(0, 0)
	public angularAceleration: number = 0
	public angularVelocity: number = 0
	public rotation: number = 0

	constructor(rotation: number) {
		this.rotation = rotation
	}

	// Calculates new position with unit's velocity
	public calcPosition(delta: number, position: Vector): Vector {
		this.applyVelocity(delta)
		return position.add(this.velocity.multiply(delta * ACCELERATION_FORCE))
	}

	// Calculates new rotation with unit's angular velocity
	public calcRotation(delta: number, rotation: number): number {
		this.applyAngularVelocity(delta)
		const newRotation =
			rotation + this.angularVelocity * delta * ROTATION_FORCE
		return normalizeRotation(newRotation)
	}

	// Applies acceleration to velicity
	private applyVelocity(delta: number): void {
		this.velocity = this.velocity
			.add(this.aceleration.multiply(delta * ACCELERATION_FORCE))
			.multiply(0.98)
			.limit(50)
	}

	// Applies angular acceleration to velicity
	private applyAngularVelocity(delta: number): void {
		this.angularVelocity += this.angularAceleration * delta * ROTATION_FORCE
		this.angularVelocity *= 0.95
		this.angularVelocity = this.limitNum(this.angularVelocity, 0)
	}

	// Calculates acceleration force
	// Force vector is in the direction unit is facing
	public accelerate(multiplier: number, rotation: number) {
		const direction = new Vector(
			Math.cos(rotation - Math.PI / 2),
			Math.sin(rotation - Math.PI / 2)
		)
		this.aceleration = new Vector(multiplier, multiplier)
			.multiplyVector(direction)
			.multiply(ACCELERATION_FORCE)
			.limit(20)
	}

	public deaccelerate() {
		this.aceleration = new Vector(0, 0)
	}

	public rotateLeft(multiplier: number) {
		this.angularAceleration = -(ROTATION_FORCE * multiplier)
	}

	public rotateRight(multiplier: number) {
		this.angularAceleration = ROTATION_FORCE * multiplier
	}

	// Rotates unit to target's direction
	// If faceBackward, rotates to the opposite side
	public rotateTo(
		unit: BaseUnit,
		target: BaseUnit,
		faceBackward: boolean = false
	): "left" | "right" | "none" {
		const angleDifference = getAngleDifference(unit, target, faceBackward)

		if (Math.abs(angleDifference) < 0.01) {
			return "none"
		}

		if (angleDifference < Math.PI) {
			return "right"
		} else {
			return "left"
		}
	}

	public stopRotation() {
		this.angularAceleration = 0
	}

	// Limits number to magnitude
	private limitNum(value: number, limit: number) {
		let normalized = Math.sqrt(Math.pow(value, 2))
		normalized = Math.max(normalized, limit)
		return normalized * Math.sign(value)
	}
}
