import * as ECSA from "../../libs/pixi-component"

import { Component, Message } from "../types"
import * as Builders from "../utils/builders"
import * as Graphics from "../utils/graphics"

class GameController extends Component {
	onInit() {
		this.subscribe("game_start")
		this.buildGame()
	}

	onMessage(msg: Message): void {
		const { action } = msg

		if (action === "game_start") {
			this.owner.asContainer().removeChildren()
			this.buildGame()
		}
	}

	onRemove() {
		this.unsubscribe("game_start")
	}

	private buildGame() {
		const scene = this.owner.scene

		const mapSize = 750
		const map = Builders.mapBuilder(scene, this.owner)().buildInto(
			Graphics.map(mapSize)
		)
		Builders.playerBuilder(scene, map)(new ECSA.Vector(mapSize)).build()
		Builders.spawnerFoodBuilder(scene, map)().build()
		Builders.spawnerDNABuilder(scene, map)().build()
		Builders.spawnerEnemyBuilder(scene, map)().build()
	}
}

export default GameController
