import EvolvableUnit from "../atributes/EvolvableUnit"
import { Component, AbilityType, UpgradeType } from "../types"

export const abilityPrices: Record<AbilityType, number[]> = {
	size: [0, 5, 15, 30, 45],
	speed: [0, 2, 4, 8, 16, 24, 32, 40, 48, 56],
	agility: [0, 1, 2, 4, 8, 12, 16, 20, 24, 28],
	strength: [0, 2, 4, 8, 16, 24, 32, 40, 48, 56],
	defense: [0, 1, 2, 4, 8, 12, 16, 20, 24, 28],
	sight: [0, 2, 4, 8, 16, 24, 32, 40, 48, 56]
}
export const abilityLengths: Record<AbilityType, number> = AbilityType.reduce(
	(acc, curr) => ({
		...acc,
		[curr]: abilityPrices[curr].length
	}),
	{} as any
)

export const upgradePrices: Record<UpgradeType, number[]> = {
	mitochondrion: [3, 9, 15],
	storage: [3, 9, 15],
	boost: [10, 20, 40],
	poison: [10, 20, 40],
	camouflage: [5, 10, 20],
	mutator: [50, 100, 200]
}
export const upgradeLengths: Record<UpgradeType, number> = UpgradeType.reduce(
	(acc, curr) => ({
		...acc,
		[curr]: upgradePrices[curr].length
	}),
	{} as any
)

export const upgradeSlots: number[] = [0, 2, 3, 3, 4]
export const upgradeModifiers: Record<UpgradeType, number[]> = {
	mitochondrion: [1.5, 2, 2.5],
	storage: [2, 4, 6],
	boost: [1.5, 3, 4.5],
	poison: [1.5, 2, 2.5],
	camouflage: [1.3, 1.6, 1.9],
	mutator: [0.1, 0.2, 0.4]
}
export const mitochondrionModifiers: number[] = [1.5, 2, 2.5]
export const storageModifiers: number[] = [2, 4, 6]
export const boostModifiers: number[] = [1.5, 3, 4.5]
export const poisonModifiers: number[] = [1.5, 2, 2.5]
export const camouflageModifiers: number[] = [1.3, 1.6, 1.9]
export const mutatorModifiers: number[] = [0.1, 0.2, 0.4]

export const applyUpgrade = (
	upgrades: Record<UpgradeType, Record<number, number>>,
	upgrade: UpgradeType
): number =>
	Object.values(upgrades[upgrade]).reduce(
		(acc, curr) => acc * upgradeModifiers[upgrade][curr - 1],
		1
	)

class UpgradeManager extends Component {
	protected unit: EvolvableUnit

	onInit(): void {
		this.unit = this.owner.getAttribute("game_unit")
	}

	protected changeAbility(ability: AbilityType, increase = true) {
		const currentValue = this.unit[ability]

		if (increase && abilityLengths[ability] <= currentValue) return
		if (!increase && currentValue <= 1) return

		let price: number
		if (increase) {
			price = abilityPrices[ability][currentValue]
		} else {
			price = abilityPrices[ability][currentValue - 1]
		}

		if (price > this.unit.dna) return
		this.unit.dna -= price

		if (increase) {
			this.unit[ability] += 1
		} else {
			this.unit[ability] -= 1
		}

		if (ability === "size") {
			this.unit.upgradeSlots += upgradeSlots[this.unit.size - 1]
		}

		this.sendMessage("evolve", { id: this.unit.id, type: ability })
	}

	protected unlock(upgrade: UpgradeType) {
		if (this.unit.upgradeSlots === 0) return

		const price = upgradePrices[upgrade][0]
		if (price > this.unit.dna) return

		const upgradeId = this.unit.addUpgrade(upgrade)
		this.unit.dna -= price
		this.unit.upgradeSlots -= 1
		this.sendMessage("evolve", {
			id: this.unit.id,
			type: upgrade,
			upgradeId
		})
	}

	protected changeUpgrade(upgrade: UpgradeType, id: number, increase = true) {
		const currentValue = this.unit.upgrades[upgrade][id]

		if (increase && upgradeLengths[upgrade] <= currentValue) return
		if (!increase && currentValue <= 1) return

		let price: number
		if (increase) {
			price = upgradePrices[upgrade][currentValue]
		} else {
			price = upgradePrices[upgrade][currentValue - 1]
		}

		if (price > this.unit.dna) return
		this.unit.dna -= price

		if (increase) {
			this.unit.upgrades[upgrade][id] += 1
		} else {
			this.unit.upgrades[upgrade][id] -= 1
		}

		this.sendMessage("evolve", {
			id: this.unit.id,
			type: upgrade,
			upgradeId: id
		})
	}
}

export default UpgradeManager
