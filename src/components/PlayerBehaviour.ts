import * as ECSA from "../../libs/pixi-component"

import EvolvableUnit from "../atributes/EvolvableUnit"
import DynamicUnitBehaviour from "./DynamicUnitBehaviour"
import { Message } from "../types"

class PlayerBehaviour extends DynamicUnitBehaviour<EvolvableUnit> {
	private inputComponent: ECSA.KeyInputComponent
	private pressedCheat: boolean = false

	onInit(): void {
		this.inputComponent = this.scene.findGlobalComponentByName<
			ECSA.KeyInputComponent
		>(ECSA.KeyInputComponent.name)
		super.onInit()
	}

	onMessage(msg: Message): void {
		const { action, data } = msg

		if (action === "death" && data.id === this.unit.id) {
			this.sendMessage("game_over")
			this.owner.remove()
		}

		super.onMessage(msg)
	}

	onUpdate(delta: number) {
		super.onUpdate(delta)
		this.findInputComponent()
		this.registerInput()
	}

	private findInputComponent() {
		if (this.inputComponent) return
		this.inputComponent = this.scene.findGlobalComponentByName<
			ECSA.KeyInputComponent
		>(ECSA.KeyInputComponent.name)
	}

	private registerInput() {
		if (!this.inputComponent) return

		if (this.inputComponent.isKeyPressed(ECSA.Keys.KEY_UP)) {
			this.isMoving = true
		} else {
			this.isMoving = false
		}

		if (this.inputComponent.isKeyPressed(ECSA.Keys.KEY_LEFT)) {
			this.rotateDirection = "left"
		} else if (this.inputComponent.isKeyPressed(ECSA.Keys.KEY_RIGHT)) {
			this.rotateDirection = "right"
		} else {
			this.rotateDirection = "none"
		}

		if (
			this.inputComponent.isKeyPressed(ECSA.Keys.KEY_X) &&
			!this.pressedCheat
		) {
			this.unit.dna += 1
			this.pressedCheat = true
			this.sendMessage("ate", { id: this.unit.id })
		} else if (
			!this.inputComponent.isKeyPressed(ECSA.Keys.KEY_X) &&
			this.pressedCheat
		) {
			this.pressedCheat = false
		}
	}
}

export default PlayerBehaviour
