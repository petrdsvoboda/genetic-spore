import Spawner from "./Spawner"
import { Message } from "../types"
import { BaseUnit } from "../atributes"

class MessageSpawner extends Spawner {
	onInit() {
		this.subscribe("death")
	}

	onMessage(msg: Message) {
		const { action, data } = msg

		if (action !== "death" || data.other.type !== "enemy") return

		this.trigger(data.other)
	}

	onRemove(): void {
		this.unsubscribe("death")
	}

	protected trigger(other: BaseUnit) {
		this.spawn(other.position)
	}
}

export default MessageSpawner
