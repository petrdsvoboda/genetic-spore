import * as ECSA from "../../libs/pixi-component"

import RandomSpawner from "./RandomSpawner"
import { FoodUnit } from "../atributes"
import * as Graphics from "../utils/graphics"

class FoodSpawner extends RandomSpawner {
	protected rate = 0.05
	private startingFood = 500

	onInit() {
		super.onInit()
		for (let i = 0; i < this.startingFood; i++) {
			this.spawn()
		}
	}

	protected spawn(): void {
		const random = Math.random() * 1000
		let size = 1
		if (random > 995) {
			size = 4
		} else if (random > 950) {
			size = 3
		} else if (random > 800) {
			size = 2
		}
		this.spawnInto = () => Graphics.food(size)

		this.builder = (position: ECSA.Vector, rotation: number) =>
			this.baseBuilder(position, rotation).withAttribute(
				"game_unit",
				new FoodUnit("food", {
					type: "food",
					position: position,
					rotation: rotation,
					size,
					scaledSize: size * 4
				})
			)

		super.spawn()
	}
}

export default FoodSpawner
