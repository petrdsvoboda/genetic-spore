import EnemyAI from "../atributes/EnemyAI"
import { Message } from "../types"
import DynamicUnitBehaviour from "./DynamicUnitBehaviour"
import EnemyUnit from "../atributes/EnemyUnit"
import { CollisionType } from "./CollisionResolver"

class EnemyBehaviour extends DynamicUnitBehaviour<EnemyUnit> {
	private ai: EnemyAI
	private timeAliveBuffer = 0

	onInit() {
		super.onInit()
		this.ai = new EnemyAI({ unit: this.unit, target: null })
	}

	onUpdate(delta: number) {
		this.countTimeAlive(delta)
		this.decideActivity()

		super.onUpdate(delta)
	}

	onCollision(otherId: number, type: CollisionType) {
		const other = this.collisionResolver.units[otherId]
		if (type === "warning" && other) {
			this.ai.send("alert", { target: other })
		}
		super.onCollision(otherId, type)
	}

	onMessage(msg: Message) {
		const { action, data } = msg

		if (action === "death") {
			if (data.id === this.unit.id) {
				this.owner.remove()
			} else if (this.ai.target && data.id === this.ai.target.id) {
				this.ai.send("lostTarget")
			}
		}

		super.onMessage(msg)
	}

	private decideActivity() {
		this.ai.send()

		if (this.ai.state === "idle" || this.ai.state === "wandering") {
			this.findTarget()
		}

		if (this.ai.state === "idle") {
			this.isMoving = false
			this.rotateDirection = "none"
		} else if (this.ai.state === "wandering") {
			this.isMoving = true
			const rnd = Math.random()
			if (rnd < 0.5) {
				this.rotateDirection = "none"
			} else if (rnd < 0.75) {
				this.rotateDirection = "right"
			} else {
				this.rotateDirection = "left"
			}
		} else if (this.ai.state === "rotatingTo") {
			this.isMoving = false
			this.rotateDirection = this.dynamics.rotateTo(
				this.unit,
				this.ai.target
			)
		} else if (this.ai.state === "rotatingFrom") {
			this.isMoving = false
			this.rotateDirection = this.dynamics.rotateTo(
				this.unit,
				this.ai.target,
				true
			)
		} else if (this.ai.state === "chasing") {
			this.isMoving = true
			this.rotateDirection = this.dynamics.rotateTo(
				this.unit,
				this.ai.target
			)
		} else if (this.ai.state === "fleeing") {
			this.isMoving = true
			this.rotateDirection = this.dynamics.rotateTo(
				this.unit,
				this.ai.target,
				true
			)
		}
	}

	private countTimeAlive(delta: number) {
		this.timeAliveBuffer += delta

		const seconds = Math.floor(this.timeAliveBuffer / 1000)
		if (seconds > 0) {
			this.timeAliveBuffer = this.timeAliveBuffer % 1000
			this.unit.timeAlive += 1
		}
	}

	private findTarget(): void {
		const target = this.collisionResolver.getNearestUnit(
			this.unit.id
		)
		if (target === null) return
		this.ai.send("foundTarget", { target })
	}
}

export default EnemyBehaviour
