import EvolvableUnit from "../atributes/EvolvableUnit"
import {
	abilityPrices,
	abilityLengths,
	upgradePrices,
	upgradeLengths
} from "../components/UpgradeManager"
import {
	Message,
	Component,
	CellType,
	AbilityType,
	UpgradeType
} from "../types"

type ButtonAction = "increase" | "decrease" | "unlock"

const getStatValue = (id: CellType): Element =>
	document.querySelector(`.stat-value[data-type="${id}"]`)

const getUpgradeStatValue = (type: UpgradeType, id: number): Element =>
	document.querySelector(`.stat-value[data-type="${type}${id}"]`)

const getButtonEl = (id: CellType, action: ButtonAction): Element =>
	document.querySelector(
		`.stat-actions[data-type="${id}"] button[data-action="${action}"]`
	)

const getUpgradesActiveEl = (): Element =>
	document.querySelector(`.stat-column[data-type="upgrades-active"]`)

class UIController extends Component {
	private unit: EvolvableUnit

	private energyValue = document.getElementById("energyValue")
	private energyMax = document.getElementById("energyMax")
	private energyRate = document.getElementById("energyRate")
	private dnaValue = document.getElementById("dnaValue")
	private dnaRate = document.getElementById("dnaRate")

	private gameOverScreen = document.getElementById("game-over")
	private gameRestartBtn = document.getElementById("game-restart")
	private totalScore = document.getElementById("total-score")

	private upgradesActiveEl = getUpgradesActiveEl()

	private abilityButtonMap: Record<
		AbilityType,
		[Element, Element]
	> = AbilityType.reduce(
		(acc, curr) => ({
			...acc,
			[curr]: [
				getButtonEl(curr, "increase"),
				getButtonEl(curr, "decrease")
			]
		}),
		{} as any
	)
	private upgradeButtonMap: Record<UpgradeType, Element> = UpgradeType.reduce(
		(acc, curr) => ({
			...acc,
			[curr]: getButtonEl(curr, "unlock")
		}),
		{} as any
	)
	private upgradeActiveButtonMap: [
		number,
		UpgradeType,
		[Element, Element]
	][] = []

	private valueMap: Record<CellType, Element> = CellType.reduce(
		(acc, curr) => ({
			...acc,
			[curr]: getStatValue(curr)
		}),
		{} as any
	)

	onInit() {
		this.subscribe("evolve", "ate", "game_over")
		this.findUnit()

		for (const ability of AbilityType) {
			const [incBtn, decBtn] = this.abilityButtonMap[ability]

			incBtn.addEventListener("click", () => {
				this.sendMessage("increase", {
					id: this.unit.id,
					type: ability
				})
			})

			decBtn.addEventListener("click", () => {
				this.sendMessage("decrease", {
					id: this.unit.id,
					type: ability
				})
			})

			this.setAbility(ability)
		}
		for (const upgrade of UpgradeType) {
			const btn = this.upgradeButtonMap[upgrade]

			btn.addEventListener("click", () => {
				this.sendMessage("unlock", {
					id: this.unit.id,
					type: upgrade
				})
			})
		}
		this.disableButtons()

		this.gameRestartBtn.addEventListener("click", () => {
			this.sendMessage("game_start")
			this.gameOverScreen.setAttribute("data-show", "false")
		})
	}

	onUpdate() {
		if (!this.unit) {
			this.findUnit()
			return
		}

		this.energyValue.innerText = Math.floor(this.unit.energy).toString()
		this.energyMax.innerText = Math.floor(this.unit.energyMax).toString()
		this.energyRate.innerText = this.unit.energyRegen.toString()
		this.dnaValue.innerText = this.unit.dna.toString()
		this.dnaRate.innerText = this.unit.dnaRegen.toString()
	}

	onMessage(msg: Message) {
		if (!this.unit) {
			this.findUnit()
			return
		}

		const { action, data } = msg

		if (action === "game_over") {
			this.gameOverScreen.setAttribute("data-show", "true")
			this.totalScore.innerText = this.unit.totalDNA.toString()
			this.unit = null
			return
		}

		if (action === "evolve" && data.id === this.unit.id) {
			if (AbilityType.includes(data.type as any)) {
				this.setAbility(data.type as any)
			} else if (UpgradeType.includes(data.type as any)) {
				this.setUpgrades(data.type as any, data.upgradeId)
			}
			this.disableButtons()
		}

		if (action === "ate" && data.id === this.unit.id) {
			this.disableButtons()
		}
	}

	onRemove(): void {
		this.unsubscribe("evolve")
		this.unsubscribe("ate")
		this.unsubscribe("game_over")
	}

	private findUnit() {
		const player = this.scene.findObjectByTag("player")
		if (player) {
			this.unit = player.getAttribute<EvolvableUnit>("game_unit")
			for (const ability of AbilityType) {
				this.setAbility(ability)
			}
			while (
				this.upgradesActiveEl.hasChildNodes &&
				this.upgradesActiveEl.firstChild
			) {
				this.upgradesActiveEl.removeChild(
					this.upgradesActiveEl.firstChild
				)
			}
			this.upgradeActiveButtonMap = []
			this.disableButtons()
		}
	}

	private getAbilityPointContent(
		index: number,
		ability: AbilityType
	): string {
		const value = this.unit[ability]
		return index < value ? "■" : "□"
	}

	private getUpgradePointContent(
		index: number,
		upgrade: UpgradeType,
		id: number
	): string {
		const value = this.unit.upgrades[upgrade][id]
		return index < value ? "■" : "□"
	}

	private setAbility(ability: AbilityType) {
		const el = this.valueMap[ability]
		//https://stackoverflow.com/questions/3955229/remove-all-child-elements-of-a-dom-node-in-javascript
		while (el.hasChildNodes && el.firstChild) {
			el.removeChild(el.firstChild)
		}

		for (let i = 0; i < abilityLengths[ability]; i++) {
			const pointEl = document.createElement("span")
			pointEl.innerText = this.getAbilityPointContent(i, ability)
			el.appendChild(pointEl)
		}
	}

	private addUpgrade(upgrade: UpgradeType, id: number): Element[] {
		const name = document.createElement("span")
		name.setAttribute("class", "stat-name")
		name.innerText = upgrade

		const value = document.createElement("div")
		value.setAttribute("class", "stat-value")
		value.setAttribute("data-type", upgrade + id)

		const actions = document.createElement("div")
		actions.setAttribute("class", "stat-actions")
		actions.setAttribute("data-type", upgrade + id)

		const decBtn = document.createElement("button")
		decBtn.setAttribute("data-action", "decrease")
		decBtn.innerText = "-"
		decBtn.addEventListener("click", () => {
			this.sendMessage("decreaseUpgrade", {
				id: this.unit.id,
				type: upgrade,
				upgradeId: id
			})
		})

		const incBtn = document.createElement("button")
		incBtn.setAttribute("data-action", "increase")
		incBtn.innerText = "+"
		incBtn.addEventListener("click", () => {
			this.sendMessage("increaseUpgrade", {
				id: this.unit.id,
				type: upgrade,
				upgradeId: id
			})
		})

		actions.appendChild(decBtn)
		actions.appendChild(incBtn)

		this.upgradeActiveButtonMap.push([id, upgrade, [decBtn, incBtn]])
		return [name, value, actions]
	}

	private setUpgrades(upgrade: UpgradeType, id: number) {
		let upgradeEl = getUpgradeStatValue(upgrade, id)
		if (!upgradeEl) {
			const statEls = this.addUpgrade(upgrade, id)
			statEls.forEach(el => this.upgradesActiveEl.appendChild(el))
			upgradeEl = getUpgradeStatValue(upgrade, id)
		}

		while (upgradeEl.hasChildNodes && upgradeEl.firstChild) {
			upgradeEl.removeChild(upgradeEl.firstChild)
		}

		for (let i = 0; i < upgradeLengths[upgrade]; i++) {
			const pointEl = document.createElement("span")
			pointEl.innerText = this.getUpgradePointContent(i, upgrade, id)
			upgradeEl.appendChild(pointEl)
		}
	}

	private disableButtons() {
		Object.values(this.abilityButtonMap).forEach(btns =>
			btns.forEach(btn => {
				btn.removeAttribute("disabled")
				btn.removeAttribute("title")
			})
		)
		Object.values(this.upgradeButtonMap).forEach(btn => {
			btn.removeAttribute("disabled")
			btn.removeAttribute("title")
		})
		Object.values(this.upgradeActiveButtonMap).forEach(data =>
			data[2].forEach(btn => {
				btn.removeAttribute("disabled")
				btn.removeAttribute("title")
			})
		)

		for (const ability of AbilityType) {
			const addBtn = this.abilityButtonMap[ability][0]
			const remBtn = this.abilityButtonMap[ability][1]

			if (this.unit[ability] < abilityLengths[ability]) {
				const nextPrice = abilityPrices[ability][this.unit[ability]]
				if (nextPrice > this.unit.dna) {
					addBtn.setAttribute("disabled", "true")
				}
				addBtn.setAttribute("title", nextPrice.toString())
			} else {
				addBtn.setAttribute("disabled", "true")
				addBtn.setAttribute("title", "Can't upgrade anymore")
			}

			if (this.unit[ability] > 1) {
				const prevPrice = abilityPrices[ability][this.unit[ability] - 1]
				if (prevPrice > this.unit.dna) {
					remBtn.setAttribute("disabled", "true")
				}
				remBtn.setAttribute("title", prevPrice.toString())
			} else {
				remBtn.setAttribute("disabled", "true")
				remBtn.setAttribute("title", "Can't be lower")
			}
		}

		for (const data of this.upgradeActiveButtonMap) {
			const [id, upgrade, [decBtn, incBtn]] = data

			if (this.unit.upgrades[upgrade][id] > 1) {
				const prevPrice =
					upgradePrices[upgrade][this.unit.upgrades[upgrade][id] - 1]
				if (prevPrice > this.unit.dna) {
					decBtn.setAttribute("disabled", "true")
				}
				decBtn.setAttribute("title", prevPrice.toString())
			} else {
				decBtn.setAttribute("disabled", "true")
				decBtn.setAttribute("title", "Can't be lower")
			}

			if (this.unit.upgrades[upgrade][id] < upgradeLengths[upgrade]) {
				const nextPrice =
					upgradePrices[upgrade][this.unit.upgrades[upgrade][id]]
				if (nextPrice > this.unit.dna) {
					incBtn.setAttribute("disabled", "true")
				}
				incBtn.setAttribute("title", nextPrice.toString())
			} else {
				incBtn.setAttribute("disabled", "true")
				incBtn.setAttribute("title", "Can't upgrade anymore")
			}
		}

		for (const upgrade of UpgradeType) {
			const btn = this.upgradeButtonMap[upgrade]

			const price = upgradePrices[upgrade][0]
			if (this.unit.upgradeSlots === 0) {
				btn.setAttribute("disabled", "true")
				btn.setAttribute("title", "No upgrade slots available")
			} else if (price > this.unit.dna) {
				btn.setAttribute("disabled", "true")
				btn.setAttribute("title", price.toString())
			} else {
				btn.setAttribute("title", price.toString())
			}
		}
	}
}

export default UIController
