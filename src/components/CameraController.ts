import * as ECSA from "../../libs/pixi-component"

import * as Graphics from "../utils/graphics"
import { Component, Message } from "../types"
import EvolvableUnit from "../atributes/EvolvableUnit"

class CameraController extends Component {
	private player: EvolvableUnit
	private blocker: ECSA.GameObject

	onInit() {
		this.findPlayer()
		this.subscribe("evolve", "game_over")
	}

	onUpdate() {
		// Finds player in game and centers camera around him
		if (!this.player) {
			this.findPlayer()
			return
		}

		this.owner.pixiObj.pivot.x = this.player.position.x
		this.owner.pixiObj.pivot.y = this.player.position.y
		this.owner.pixiObj.rotation = -this.player.rotation

		if (!this.blocker) {
			this.setBlocker()
		} else {
			this.blocker.pixiObj.position.x = this.player.position.x
			this.blocker.pixiObj.position.y = this.player.position.y
		}
	}

	onMessage(msg: Message) {
		const { action, data } = msg
		// Reduces view around player based on his sight
		if (
			this.player &&
			action === "evolve" &&
			data.id === this.player.id &&
			data.type === "sight"
		) {
			this.setBlocker()
		} else if (action === "game_over") {
			this.player = null
			this.blocker = null
		}
	}

	onRemove() {
		this.unsubscribe("evolve")
		this.unsubscribe("game_over")
	}

	private findPlayer(): void {
		if (this.player) return
		const playerObject = this.scene.findObjectByTag("player")
		if (!playerObject) return
		this.player = playerObject.getAttribute("game_unit")
	}

	private setBlocker(): void {
		if (this.blocker) {
			this.owner.pixiObj.removeChild(this.blocker as any)
		}

		const sightRadius = this.player.sightRadius
		this.blocker = new ECSA.Builder(this.scene)
			.withParent(this.owner)
			.localPos(this.player.position.x, this.player.position.y)
			.anchor(0.5)
			.buildInto(
				Graphics.sightBlocker(
					this.scene.stage.pixiObj.width,
					this.scene.stage.pixiObj.height,
					this.player.scaledSize,
					sightRadius
				)
			)
	}
}

export default CameraController
