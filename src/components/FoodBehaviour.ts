import FoodUnit from "../atributes/FoodUnit"
import UnitBehaviour from "./UnitBehaviour"

class FoodBehaviour extends UnitBehaviour<FoodUnit> {
	onCollision() {
		this.sendMessage("death", { id: this.unit.id, other: this.unit })
		if (this.owner) {
			this.owner.remove()
		}
	}
}

export default FoodBehaviour
