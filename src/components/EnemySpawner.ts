import * as ECSA from "../../libs/pixi-component"

import RandomSpawner from "./RandomSpawner"
import { EnemyUnit } from "../atributes"
import { Message, AbilityType } from "../types"
import { BuildObjectFn } from "../utils/builders"
import { random } from "../utils/upgrades"
import { EnemyUnitData } from "../atributes/EnemyUnit"
import { abilityLengths } from "./UpgradeManager"

const initEnemyData: EnemyUnitData = {
	size: 1,
	strength: 1,
	speed: 1,
	agility: 1,
	defense: 1,
	sight: 1,
	dna: random(5),
	aggressivity: Math.random(),
	curiosity: Math.random(),
	courage: Math.random(),
	intelligence: Math.random(),
	greediness: Math.random()
}

class EnemySpawner extends RandomSpawner {
	protected rate: number = 5

	private population: number = 0
	private maxPopulation: number = 50

	private tournamentSize: number = 6
	private crossoverProbability: number = 0.5
	private mutationProbability: number = 0.05

	onInit() {
		super.onInit()
		this.subscribe("death")
		this.generateInitialPopulation()
	}

	onMessage(msg: Message) {
		const { action, data } = msg

		if (action !== "death") return

		if (data.other.type === "enemy") {
			this.population -= 1
			this.generateNewUnit()
		}
	}

	onUpdate(delta: number) {
		if (this.population === this.maxPopulation) return
		super.onUpdate(delta)
	}

	onRemove(): void {
		this.unsubscribe("death")
	}

	public generateInitialPopulation() {
		for (let i = 0; i < this.maxPopulation; i++) {
			this.spawnEnemy(initEnemyData)
		}
	}

	public randomAttr(a: number, b: number) {
		return Math.random() > 0.5 ? a : b
	}

	public mutateAttr(type: AbilityType, a: number, decimal = false) {
		const trigger = Math.random() < this.mutationProbability
		if (!trigger) return

		if (decimal) {
			return Math.random()
		} else {
			if (a === 1) return 2
			if (a === abilityLengths[type] - 1) return a - 1

			return Math.random() > 0.5 ? a + 1 : a - 1
		}
	}

	private generateNewUnit() {
		const population = this.getPopulation()
		const bestDNA = population.reduce(
			(acc, curr) => (acc > curr.totalDNA ? acc : curr.totalDNA),
			0
		)
		const bestAlive = population.reduce(
			(acc, curr) => (acc > curr.timeAlive ? acc : curr.timeAlive),
			0
		)

		let unitData: EnemyUnitData
		if (this.crossoverProbability < Math.random()) {
			const a = this.tournament(population, bestDNA, bestAlive)
			const b = this.tournament(population, bestDNA, bestAlive)

			unitData = {
				dna: this.randomAttr(a.dna, b.dna),
				aggressivity: this.randomAttr(a.aggressivity, b.aggressivity),
				curiosity: this.randomAttr(a.curiosity, b.curiosity),
				courage: this.randomAttr(a.courage, b.courage),
				intelligence: this.randomAttr(a.intelligence, b.intelligence),
				greediness: this.randomAttr(a.greediness, b.greediness),
				size: this.randomAttr(a.size, b.size),
				strength: this.randomAttr(a.strength, b.strength),
				speed: this.randomAttr(a.speed, b.speed),
				agility: this.randomAttr(a.agility, b.agility),
				defense: this.randomAttr(a.defense, b.defense),
				sight: this.randomAttr(a.sight, b.sight)
			}
		} else {
			const unit = this.tournament(population, bestDNA, bestAlive)
			unitData = {
				dna: unit.dna,
				aggressivity: unit.aggressivity,
				curiosity: unit.curiosity,
				courage: unit.courage,
				intelligence: unit.intelligence,
				greediness: unit.greediness,
				size: unit.size,
				strength: unit.strength,
				speed: unit.speed,
				agility: unit.agility,
				defense: unit.defense,
				sight: unit.sight
			}
		}

		this.spawnEnemy(unitData)
	}

	private getPopulation(): EnemyUnit[] {
		return this.owner
			.asContainer()
			.scene.findObjectsByTag("enemy")
			.map(enemy => enemy.getAttribute("game_unit"))
	}

	private fitness(
		unit: EnemyUnit,
		bestDNA: number,
		bestAlive: number
	): number {
		return (unit.totalDNA / bestDNA) * (unit.timeAlive / bestAlive)
	}

	private tournament(
		population: EnemyUnit[],
		bestDNA: number,
		bestAlive: number
	): EnemyUnit {
		let units: EnemyUnit[] = []
		for (let i = 0; i < this.tournamentSize; i++) {
			const index = random(population.length)
			units.push(population[index])
		}

		return units.reduce(
			(acc, curr) =>
				this.fitness(acc, bestDNA, bestAlive) >
				this.fitness(curr, bestDNA, bestAlive)
					? acc
					: curr,
			units[0]
		)
	}

	private spawnEnemy(unitData: EnemyUnitData): void {
		this.builder = (position: ECSA.Vector, rotation: number) =>
			this.baseBuilder(position, rotation).withAttribute(
				"game_unit",
				new EnemyUnit({
					type: "enemy",
					position: position,
					rotation: rotation,
					...unitData
				})
			)

		this.population += 1
		super.spawn()
	}
}

export default EnemySpawner
