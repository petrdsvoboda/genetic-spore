import { Message } from "../types"
import UpgradeManager from "./UpgradeManager"

class PlayerUpgradeManager extends UpgradeManager {
	onInit(): void {
		this.subscribe(
			"increase",
			"decrease",
			"unlock",
			"increaseUpgrade",
			"decreaseUpgrade"
		)
		super.onInit()
	}

	onMessage(msg: Message) {
		const { action, data } = msg

		if (data.id !== this.unit.id) return

		if (action === "increase") {
			this.changeAbility(data.type as any)
		} else if (action === "decrease") {
			this.changeAbility(data.type as any, false)
		} else if (action === "unlock") {
			this.unlock(data.type as any)
		} else if (action === "increaseUpgrade") {
			this.changeUpgrade(data.type as any, data.upgradeId)
		} else if (action === "decreaseUpgrade") {
			this.changeUpgrade(data.type as any, data.upgradeId, false)
		}
	}

	onRemove(): void {
		this.unsubscribe("increase")
		this.unsubscribe("decrease")
		this.unsubscribe("unlock")
		this.unsubscribe("increaseUpgrade")
		this.unsubscribe("decreaseUpgrade")
	}
}

export default PlayerUpgradeManager
