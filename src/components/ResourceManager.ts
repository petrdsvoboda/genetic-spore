import * as ECSA from "../../libs/pixi-component"

import EvolvableUnit from "../atributes/EvolvableUnit"
import { mutatorModifiers } from "./UpgradeManager"
import { UpgradeType } from "../types"

const sizeEnergy: number[] = [0, 1, 2, 4, 16]
const strengthEnergy: number[] = [0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8]
const speedEnergy: number[] = [4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048]
const agilityEnergy: number[] = [2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]
const defenseEnergy: number[] = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
const sightEnergy: number[] = [0, 0.4, 0.8, 1.2, 1.6, 2, 2.4, 2.8, 3.2, 3.6]

export const upgradeEnergy: Pick<
	Record<UpgradeType, number[]>,
	"storage" | "poison" | "camouflage" | "mutator"
> = {
	storage: [4, 6, 8],
	poison: [2.5, 5, 7.5],
	camouflage: [1.5, 2, 2.5],
	mutator: [40, 80, 120]
}
export const upgradeModifier: Pick<
	Record<UpgradeType, number[]>,
	"mitochondrion" | "boost"
> = {
	mitochondrion: [1.5, 3, 4.5],
	boost: [1.5, 3, 4.5]
}

export const applyEnergy = (
	upgrades: Record<UpgradeType, Record<number, number>>,
	upgrade: UpgradeType
): number =>
	Object.values(upgrades[upgrade]).reduce(
		(acc, curr) => acc + upgradeEnergy[upgrade][curr - 1],
		0
	)
export const applyModifier = (
	upgrades: Record<UpgradeType, Record<number, number>>,
	upgrade: UpgradeType
): number =>
	Object.values(upgrades[upgrade]).reduce(
		(acc, curr) => acc * upgradeModifier[upgrade][curr - 1],
		1
	)

class ResourceManager extends ECSA.Component {
	private unit: EvolvableUnit

	private timeBuffer: number = 0

	private baseEnergyRegen = 1
	private baseEnergyCons = 1

	get energyRegen(): number {
		const regen =
			this.baseEnergyRegen *
			applyModifier(this.unit.upgrades, "mitochondrion")

		const cons =
			this.baseEnergyCons * sizeEnergy[this.unit.size - 1] +
			this.baseEnergyCons * strengthEnergy[this.unit.strength - 1] +
			this.baseEnergyCons * defenseEnergy[this.unit.defense - 1] +
			this.baseEnergyCons * sightEnergy[this.unit.sight - 1] +
			this.baseEnergyCons * applyEnergy(this.unit.upgrades, "poison") +
			this.baseEnergyCons *
				applyEnergy(this.unit.upgrades, "camouflage") +
			this.baseEnergyCons * applyEnergy(this.unit.upgrades, "mutator")

		return Math.round((regen - cons) * 100) / 100
	}

	get dnaRegen() {
		return Object.values(this.unit.upgrades.mutator).reduce(
			(acc, curr) => acc + mutatorModifiers[curr - 1],
			0
		)
	}

	onInit() {
		this.unit = this.owner.getAttribute("game_unit")
	}

	onUpdate(delta: number) {
		this.checkDeath()

		this.unit.energyRegen = this.energyRegen
		this.unit.dnaRegen = this.dnaRegen

		this.timeBuffer += delta
		const seconds = Math.floor(this.timeBuffer / 1000)
		if (seconds > 0) {
			this.timeBuffer = this.timeBuffer % 1000
			this.generateEnergy(seconds)
			this.generateDNA(seconds)
		}
	}

	public propelEnergy(delta: number): boolean {
		const energy =
			delta *
			0.001 *
			speedEnergy[this.unit.speed - 1] *
			applyModifier(this.unit.upgrades, "boost")

		if (energy < this.unit.energy - 1) {
			this.unit.energy -= energy
			return true
		}
		return false
	}

	public rotateEnergy(delta: number): boolean {
		const energy = delta * 0.001 * agilityEnergy[this.unit.agility - 1]

		if (energy < this.unit.energy - 1) {
			this.unit.energy -= energy
			return true
		}
		return false
	}

	public addEnergy(value: number): void {
		this.unit.energy += value
		if (this.unit.energy > this.unit.energyMax) {
			this.unit.energy = this.unit.energyMax
		}
	}

	public removeEnergy(value: number): void {
		this.unit.energy -= value
	}

	private generateEnergy(seconds: number): void {
		this.unit.energy += seconds * this.energyRegen
		if (this.unit.energy > this.unit.energyMax) {
			this.unit.energy = this.unit.energyMax
		}
	}

	private generateDNA(seconds: number): void {
		this.unit.dna += seconds * this.dnaRegen
	}

	private checkDeath(): void {
		if (this.unit.energy > 0) return

		this.sendMessage("death", { id: this.unit.id, other: this.unit })
	}
}

export default ResourceManager
