import * as ECSA from "../../libs/pixi-component"

import BaseUnit from "../atributes/BaseUnit"
import Dynamics from "../atributes/Dynamics"
import EvolvableUnit from "../atributes/EvolvableUnit"
import Map from "../atributes/Map"
import ResourceManager from "./ResourceManager"
import { boostModifiers, poisonModifiers, applyUpgrade } from "./UpgradeManager"
import UnitBehaviour from "./UnitBehaviour"
import FoodUnit from "../atributes/FoodUnit"
import { CollisionType } from "./CollisionResolver"

class DynamicUnitBehaviour<T extends EvolvableUnit> extends UnitBehaviour<T> {
	protected dynamics: Dynamics

	protected isMoving: boolean = false
	protected isBoosting: boolean = false
	protected rotateDirection: "left" | "right" | "none" = "none"

	private map: Map

	protected resourceManager: ResourceManager

	onInit(): void {
		this.resourceManager = this.owner.findComponentByName<ResourceManager>(
			"ResourceManager"
		)
		super.onInit()
		this.subscribe("death")
		this.dynamics = new Dynamics(this.owner.pixiObj.rotation)
		this.map = this.owner.parentGameObject.getAttribute<Map>("map")
	}

	onCollision(otherId: number, type: CollisionType) {
		const other = this.collisionResolver.units[otherId]
		if (!other) return

		if (other.type === "food") {
			const foodType = (other as FoodUnit).foodType

			if (foodType === "food") {
				this.resourceManager.addEnergy(Math.pow(10, other.size))
			} else if (foodType === "dna") {
				this.unit.dna += Math.round((other as FoodUnit).value / 2)
				this.sendMessage("ate", { id: this.unit.id })
			}
		} else if (
			(other.type === "enemy" || other.type === "player") &&
			type === "normal"
		) {
			const o = other as EvolvableUnit
			this.stayOutOfBounds(o.scaledSize, o.position)

			const strengthDiff = o.strength - this.unit.strength
			if (strengthDiff >= -2) {
				const multiplier =
					Math.pow(2, strengthDiff) *
					(1 / this.unit.defense) *
					applyUpgrade(this.unit.upgrades, "poison")
				this.resourceManager.removeEnergy(multiplier * 0.1)
			}
		}
	}

	onRemove(): void {
		this.unsubscribe("death")
		super.onRemove()
	}

	private stayOutOfBounds(size: number, center: ECSA.Vector): void {
		const offsetSize = size + this.unit.scaledSize
		const distance = center.distance(this.unit.position)

		if (distance < offsetSize) {
			let centerToPosition = this.unit.position.subtract(center)
			centerToPosition = centerToPosition.normalize()
			this.unit.position = center.add(
				centerToPosition.multiply(offsetSize)
			)
		}
	}
	private stayInBounds(size: number, center: ECSA.Vector): void {
		const offsetSize = size - this.unit.scaledSize
		const distance = center.distance(this.unit.position)

		if (distance > offsetSize) {
			let centerToPosition = this.unit.position.subtract(center)
			centerToPosition = centerToPosition.normalize()
			this.unit.position = center.add(
				centerToPosition.multiply(offsetSize)
			)
		}
	}

	private move(delta: number) {
		this.isBoosting = true
		if (this.isMoving && this.resourceManager.propelEnergy(delta)) {
			const multiplier = Object.values(this.unit.upgrades.boost).reduce(
				(acc, curr) => acc * boostModifiers[curr - 1],
				1
			)
			this.dynamics.accelerate(
				this.unit.speed * multiplier,
				this.unit.rotation
			)
		} else {
			this.dynamics.deaccelerate()
		}

		if (
			this.rotateDirection === "left" &&
			this.resourceManager.rotateEnergy(delta)
		) {
			this.dynamics.rotateLeft(this.unit.agility)
		} else if (
			this.rotateDirection === "right" &&
			this.resourceManager.rotateEnergy(delta)
		) {
			this.dynamics.rotateRight(this.unit.agility)
		} else {
			this.dynamics.stopRotation()
		}

		this.unit.position = this.dynamics.calcPosition(
			delta,
			this.unit.position
		)
		this.unit.rotation = this.dynamics.calcRotation(
			delta,
			this.unit.rotation
		)

		this.stayInBounds(this.map.size, this.map.center)
	}

	onUpdate(delta: number) {
		this.move(delta)
	}
}

export default DynamicUnitBehaviour
