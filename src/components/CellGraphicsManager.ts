import * as ECSA from "../../libs/pixi-component"

import EvolvableUnit from "../atributes/EvolvableUnit"
import { Component, Message, UpgradeType } from "../types"
import * as Graphics from "../utils/graphics"
import { upgradeLengths, abilityLengths } from "./UpgradeManager"

enum CellDataValue {
	Type,
	Id,
	Cell
}
type CellData = [UpgradeType, number, ECSA.Container]

const upgradePositions: ECSA.Vector[] = [
	new ECSA.Vector(-18, 0),
	new ECSA.Vector(18, 0),
	new ECSA.Vector(0, -28),
	new ECSA.Vector(-22, -22),
	new ECSA.Vector(22, -22),
	new ECSA.Vector(0, 22),
	new ECSA.Vector(-22, 28),
	new ECSA.Vector(22, 28),
	new ECSA.Vector(-40, 12),
	new ECSA.Vector(40, 12),
	new ECSA.Vector(-48, -12),
	new ECSA.Vector(48, -12)
]

class CellGraphicsManager extends Component {
	private unit: EvolvableUnit

	private sizeCell: ECSA.Container
	private speedCell: ECSA.Container
	private agilityCell: ECSA.Container
	private strengthCell: ECSA.Container
	private defenseCell: ECSA.Container
	private sightCell: ECSA.Container

	private upgradeCells: CellData[] = []

	onInit(): void {
		this.subscribe("evolve")
		this.unit = this.owner.getAttribute("game_unit")

		this.createSizeCell()
	}

	onMessage(msg: Message) {
		const { action, data } = msg

		if (action !== "evolve" || data.id !== this.unit.id) return

		if (data.type === "size") {
			this.createSizeCell()
		} else if (data.type === "speed") {
			this.createSpeedCell()
		} else if (data.type === "agility") {
			this.createAgilityCell()
		} else if (data.type === "strength") {
			this.createStrengthCell()
		} else if (data.type === "defense") {
			this.createDefenseCell()
		} else if (data.type === "sight") {
			this.createSightCell()
		} else if (UpgradeType.includes(data.type as any)) {
			this.updateUpgradeCell(data.upgradeId, data.type as any)
		}
	}

	onRemove(): void {
		this.unsubscribe("evolve")
	}

	private createSizeCell(): void {
		this.owner.pixiObj.removeChild(this.sizeCell)
		this.sizeCell = Graphics.sizeCell(this.unit.scaledSize)
		this.owner.pixiObj.addChild(this.sizeCell)
		this.createStrengthCell()
		this.createSpeedCell()
		this.createAgilityCell()
		this.createDefenseCell()
		this.createSightCell()

		for (const cell of this.upgradeCells) {
			this.updateUpgradeCell(
				cell[CellDataValue.Id],
				cell[CellDataValue.Type]
			)
		}
	}

	private createStrengthCell(): void {
		this.owner.pixiObj.removeChild(this.strengthCell)
		this.strengthCell = Graphics.strengthCell(
			this.unit.scaledSize,
			this.unit.strength
		)
		this.owner.pixiObj.addChild(this.strengthCell)
	}

	private createSpeedCell(): void {
		this.owner.pixiObj.removeChild(this.speedCell)
		this.speedCell = Graphics.speedCell(
			this.unit.scaledSize,
			this.unit.speed
		)
		this.owner.pixiObj.addChild(this.speedCell)
		this.createAgilityCell()
		for (const cell of this.upgradeCells) {
			if (cell[CellDataValue.Type] !== "boost") continue
			this.updateUpgradeCell(
				cell[CellDataValue.Id],
				cell[CellDataValue.Type]
			)
		}
	}

	private createAgilityCell(): void {
		this.owner.pixiObj.removeChild(this.agilityCell)
		this.agilityCell = Graphics.agilityCell(
			this.unit.scaledSize,
			this.unit.speed,
			this.unit.agility
		)
		this.owner.pixiObj.addChild(this.agilityCell)
	}

	private createDefenseCell(): void {
		this.owner.pixiObj.removeChild(this.defenseCell)
		this.defenseCell = Graphics.defenseCell(
			this.unit.scaledSize,
			this.unit.defense
		)
		this.owner.pixiObj.addChild(this.defenseCell)
	}

	private createSightCell(): void {
		this.owner.pixiObj.removeChild(this.sightCell)
		this.sightCell = Graphics.sightCell(
			this.unit.scaledSize,
			this.unit.sight
		)
		this.owner.pixiObj.addChild(this.sightCell)
	}

	private findUpgradeCell(id: number): [-1, undefined]
	private findUpgradeCell(id: number): [number, CellData]
	private findUpgradeCell(id: number): [-1, undefined] | [number, CellData] {
		const index = this.upgradeCells.findIndex(
			data => data[CellDataValue.Id] === id
		)
		return [index, this.upgradeCells[index]]
	}

	private updateUpgradeCell(id: number, type: UpgradeType): void {
		let [index, data] = this.findUpgradeCell(id)

		const cell = this.createUpgradeCell(index, id, type)

		let cellData: CellData
		if (index === -1) {
			cellData = [type, id, cell]
			this.upgradeCells.push(cellData)
		} else {
			cellData = data
			this.owner.pixiObj.removeChild(cellData[CellDataValue.Cell])
			this.upgradeCells[index][2] = cell
		}

		this.owner.pixiObj.addChild(cellData[CellDataValue.Cell])
	}

	private createUpgradeCell(
		index: number,
		id: number,
		type: UpgradeType
	): ECSA.Container {
		index = index === -1 ? this.upgradeCells.length : index

		if (
			type === "mitochondrion" ||
			type === "storage" ||
			type === "mutator"
		) {
			let graphics: typeof Graphics.mitochondrionCell
			if (type === "mitochondrion") {
				graphics = Graphics.mitochondrionCell
			} else if (type === "storage") {
				graphics = Graphics.storageCell
			} else if (type === "mutator") {
				graphics = Graphics.mutatorCell
			}

			return graphics(
				upgradePositions[index],
				this.unit.scaledSize,
				this.unit.upgrades[type][id] * 5
			)
		} else if (type === "poison" || type === "camouflage") {
			return Graphics.poisonCell(
				this.unit.scaledSize,
				this.unit.upgrades[type][id],
				upgradeLengths[type]
			)
		} else if (type === "boost") {
			return Graphics.boostCell(
				this.unit.scaledSize,
				this.unit.upgrades[type][id],
				this.unit.speed
			)
		}
	}
}

export default CellGraphicsManager
