import * as ECSA from "../../libs/pixi-component"

import { Component } from "../types"
import { BuildObjectFn } from "../utils/builders"

class Spawner extends Component {
	protected builder: BuildObjectFn

	constructor(
		protected baseBuilder: BuildObjectFn,
		protected spawnInto?: () => ECSA.Graphics
	) {
		super()
		this.builder = this.baseBuilder
	}

	protected spawn(position: ECSA.Vector, rotation?: number): void {
		if (this.spawnInto) {
			this.builder(position, rotation).buildInto(this.spawnInto())
		} else {
			this.builder(position, rotation).build()
		}
	}
}

export default Spawner
