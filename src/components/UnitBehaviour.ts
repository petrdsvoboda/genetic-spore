import BaseUnit from "../atributes/BaseUnit"
import CollisionResolver, { CollisionType } from "./CollisionResolver"
import { Component } from "../types"

class UnitBehaviour<T extends BaseUnit> extends Component {
	public unit: T
	protected collisionResolver: CollisionResolver

	onInit(): void {
		this.unit = this.owner.getAttribute("game_unit")
		this.unit.pixiObject = this.owner.pixiObj
		this.collisionResolver = this.scene.findGlobalComponentByName<
			CollisionResolver
		>("CollisionResolver")
		this.collisionResolver.addUnit(this.unit)
		this.subscribe("collision" + this.unit.id)
	}

	onRemove() {
		this.collisionResolver.removeUnit(this.unit)
		this.unsubscribe("collision" + this.unit.id)
	}

	onMessage(msg: any) {
		if (msg.action === "collision" + this.unit.id) {
			this.onCollision(msg.data.other, msg.data.type)
		}
	}

	public onCollision(otherId: number, type: CollisionType): void {}
}

export default UnitBehaviour
