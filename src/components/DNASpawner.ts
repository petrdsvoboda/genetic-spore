import * as ECSA from "../../libs/pixi-component"

import { FoodUnit, EvolvableUnit } from "../atributes"
import * as Graphics from "../utils/graphics"
import MessageSpawner from "./MessageSpawner"

class DNASpawner extends MessageSpawner {
	protected trigger(other: EvolvableUnit) {
		this.spawnInto = () => Graphics.dna(other.size)

		this.builder = (position: ECSA.Vector, rotation: number) =>
			this.baseBuilder(position, rotation).withAttribute(
				"game_unit",
				new FoodUnit("dna", {
					type: "food",
					position: position,
					rotation: rotation,
					scaledSize: other.size + 8,
					size: other.size,
					value: other.totalDNA
				})
			)

		super.trigger(other)
	}
}

export default DNASpawner
