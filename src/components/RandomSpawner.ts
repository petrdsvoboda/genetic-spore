import * as ECSA from "../../libs/pixi-component"

import Spawner from "./Spawner"
import Map from "../atributes/Map"

class RandomSpawner extends Spawner {
	protected rate = 0.5

	private map: Map
	private spawnBuffer: number = 0

	onInit() {
		this.map = this.scene.findObjectByTag("map").getAttribute("map")
	}

	onUpdate(delta: number) {
		this.spawnBuffer += delta

		if (this.spawnBuffer > this.rate * 1000) {
			this.spawn()
			this.spawnBuffer = 0
		}
	}

	protected spawn(): void {
		// Uniform distribution in circle
		// https://stackoverflow.com/questions/5837572/generate-a-random-point-within-a-circle-uniformly
		const r = this.map.size * Math.sqrt(Math.random())
		const theta = Math.random() * 2 * Math.PI

		const x = this.map.center.x + r * Math.cos(theta)
		const y = this.map.center.y + r * Math.sin(theta)
		const position = new ECSA.Vector(x, y)
		const rotation = Math.random() * 2 * Math.PI

		super.spawn(position, rotation)
	}
}

export default RandomSpawner
