import * as ECSA from "../../libs/pixi-component"

import BaseUnit from "../atributes/BaseUnit"
import { UnitType, Message } from "../types"
import { flatMap } from "../utils/functions"
import EvolvableUnit from "../atributes/EvolvableUnit"

export const CollisionType = ["normal", "warning"] as const
export type CollisionType = typeof CollisionType[number]

class CollisionResolver extends ECSA.Component {
	public units: Record<number, BaseUnit> = {}

	public addUnit(unit: BaseUnit): void {
		this.units[unit.id] = unit
	}

	public removeUnit(unit: BaseUnit): void {
		delete this.units[unit.id]
	}

	public getNearestUnit(id: number): BaseUnit | null {
		const nearby = Object.values(this.allStaticUnits).reduce<number[]>(
			(acc, curr) =>
				this.inCollision(id, curr, "warning") ? [...acc, curr] : acc,
			[]
		)

		const unitPosition = this.units[id].position
		const nearest = nearby.reduce<BaseUnit | null>(
			(acc, curr) =>
				acc === null
					? this.units[curr]
					: unitPosition.distance(this.units[curr].position) <
					  unitPosition.distance(acc.position)
					? this.units[curr]
					: acc,
			null
		)

		return nearest
	}

	onInit() {
		this.subscribe("game_start")
		super.onInit()
	}

	onUpdate(delta: number) {
		this.checkAllCollisions()
	}

	onRemove() {
		this.unsubscribe("game_start")
		super.onRemove()
	}

	onMessage({ action }: Message) {
		if (action === "game_start") {
			this.units = {}
		}
	}

	private inCollision(
		a: number,
		b: number,
		type: CollisionType = "normal"
	): boolean {
		const aUnit = this.units[a]
		const bUnit = this.units[b]
		let aSize = aUnit.scaledSize

		if (type === "warning" && aUnit instanceof EvolvableUnit) {
			aSize += aUnit.sightRadius
		}

		// https://developer.mozilla.org/en-US/docs/Games/Techniques/2D_collision_detection
		return (
			aUnit.position.distance(bUnit.position) < aSize + bUnit.scaledSize
		)
	}

	private checkCollisions(
		cmps: number[],
		targetCmps: number[],
		type: CollisionType = "normal"
	): void {
		const collisions = flatMap(cmps, cmp =>
			targetCmps
				.filter(
					target =>
						cmp !== target && this.inCollision(cmp, target, type)
				)
				.map(target => [cmp, target])
		)

		collisions.forEach(c => {
			this.sendMessage("collision" + c[0], {
				id: c[0],
				other: c[1],
				type
			})
			// Warn only unit which has required sight
			if (type !== "warning") {
				this.sendMessage("collision" + c[1], {
					id: c[1],
					other: c[0],
					type
				})
			}
		})
	}

	private checkAllCollisions(): void {
		this.checkCollisions(this.allDynamicUnits, this.allStaticUnits)
		this.checkCollisions(this.allDynamicUnits, this.allDynamicUnits)
		this.checkCollisions(
			this.allDynamicUnits,
			this.allDynamicUnits,
			"warning"
		)
	}

	private get allDynamicUnits() {
		return Object.values(this.units)
			.filter(u => u.type === "player")
			.concat(Object.values(this.units).filter(u => u.type === "enemy"))
			.map(u => u.id)
	}

	private get allStaticUnits() {
		return Object.values(this.units)
			.filter(u => u.type === "food")
			.map(u => u.id)
	}
}

export default CollisionResolver
