import UpgradeManager, {
	abilityPrices,
	upgradePrices,
	abilityLengths,
	upgradeLengths
} from "./UpgradeManager"
import { Message, AbilityType, UpgradeType } from "../types"
import { EnemyUnit } from "../atributes"

export const random = (max: number) => Math.floor(Math.random() * max)

class EnemyUpgradeManager extends UpgradeManager {
	protected unit: EnemyUnit

	onInit(): void {
		this.subscribe("ate")
		super.onInit()
		this.improve()
	}

	onMessage(msg: Message) {
		const { action, data } = msg

		if (action !== "ate" || data.id !== this.unit.id) return

		this.improve()
	}

	onRemove(): void {
		this.unsubscribe("ate")
	}

	private improve() {
		if (Math.random() < this.unit.greediness) return

		const abilities = this.getAbilityImprovements()
		const upgrades = this.getUpgradeImprovements()
		const unlocks = this.getUnlocks()

		const length = abilities.length + upgrades.length + unlocks.length
		if (length === 0) return

		const index = random(length)
		if (index < abilities.length) {
			this.changeAbility(abilities[index])
		} else if (index < abilities.length + upgrades.length) {
			const [upgrade, id] = upgrades[index - abilities.length]
			this.changeUpgrade(upgrade, id)
		} else {
			this.unlock(unlocks[index - abilities.length - upgrades.length])
		}
		this.improve()
	}

	private getAbilityImprovements(): AbilityType[] {
		const improvable: AbilityType[] = []
		for (const ability of AbilityType) {
			const current = this.unit[ability]
			if (
				abilityPrices[ability][current] > this.unit.dna ||
				abilityLengths[ability] <= current
			) {
				continue
			}
			improvable.push(ability)
		}
		return improvable
	}

	private getUpgradeImprovements(): [UpgradeType, number][] {
		const improvable: [UpgradeType, number][] = []
		for (const upgrade of UpgradeType) {
			const cells = this.unit.upgrades[upgrade]
			for (const id of Object.keys(cells)) {
				const current = cells[id]
				if (
					upgradePrices[upgrade][current] > this.unit.dna ||
					upgradeLengths[upgrade] <= current
				) {
					continue
				}
				improvable.push([upgrade, parseInt(id)])
			}
		}
		return improvable
	}

	private getUnlocks(): UpgradeType[] {
		if (this.unit.upgradeSlots === 0) return []
		const unlockable: UpgradeType[] = []
		for (const upgrade of UpgradeType) {
			if (upgradePrices[upgrade][0] > this.unit.dna) continue
			unlockable.push(upgrade)
		}
		return unlockable
	}
}

export default EnemyUpgradeManager
