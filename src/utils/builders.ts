import * as ECSA from "../../libs/pixi-component"

import EvolvableUnit from "../atributes/EvolvableUnit"
import Map from "../atributes/Map"
import * as Components from "../components"

export type BuildObjectFn = (
	position?: ECSA.Vector,
	rotation?: number,
	size?: number
) => ECSA.Builder
export type CreateBuilderFn = (
	scene: ECSA.Scene,
	parent: ECSA.GameObject
) => BuildObjectFn

const baseBuilder: CreateBuilderFn = (scene, parent) => () => {
	return new ECSA.Builder(scene).anchor(0.5).withParent(parent)
}

const evolvableUnitBuilder: CreateBuilderFn = (scene, parent) => () => {
	return baseBuilder(scene, parent)()
		.withComponent(new Components.ResourceManager())
		.withComponent(new Components.CellGraphicsManager())
}

export const playerBuilder: CreateBuilderFn = (scene, parent) => position => {
	const unit = new EvolvableUnit({
		type: "player",
		position: position,
		size: 1
	})

	return evolvableUnitBuilder(scene, parent)()
		.withTag("player")
		.withAttribute("game_unit", unit)
		.withComponent(new Components.PlayerUpgradeManager())
		.withComponent(new Components.PlayerBehaviour())
}

export const enemyBuilder = (scene, parent) => () => {
	return evolvableUnitBuilder(scene, parent)()
		.withTag("enemy")
		.withComponent(new Components.EnemyUpgradeManager())
		.withComponent(new Components.EnemyBehaviour())
}

export const spawnerEnemyBuilder: CreateBuilderFn = (scene, parent) => () => {
	return new ECSA.Builder(scene)
		.withComponent(new Components.EnemySpawner(enemyBuilder(scene, parent)))
		.localPos(0, 0)
		.anchor(0)
		.withParent(parent)
}

export const foodBuilder: CreateBuilderFn = (scene, parent) => position => {
	return baseBuilder(scene, parent)()
		.withTag("food")
		.withComponent(new Components.FoodBehaviour())
		.localPos(position)
}

export const spawnerFoodBuilder: CreateBuilderFn = (scene, parent) => () => {
	return new ECSA.Builder(scene)
		.withComponent(new Components.FoodSpawner(foodBuilder(scene, parent)))
		.localPos(0, 0)
		.anchor(0)
		.withParent(parent)
}

export const DNABuilder: CreateBuilderFn = (scene, parent) => position => {
	return baseBuilder(scene, parent)()
		.withTag("food")
		.withComponent(new Components.FoodBehaviour())
		.localPos(position)
}

export const spawnerDNABuilder: CreateBuilderFn = (scene, parent) => () => {
	return new ECSA.Builder(scene)
		.withComponent(new Components.DNASpawner(DNABuilder(scene, parent)))
		.localPos(0, 0)
		.anchor(0)
		.withParent(parent)
}

export const collisionResolverBuilder: CreateBuilderFn = (
	scene,
	parent
) => () => {
	return new ECSA.Builder(scene)
		.withComponent(new Components.CollisionResolver())
		.localPos(0, 0)
		.anchor(0)
		.withParent(parent)
}

export const mapBuilder: CreateBuilderFn = (scene, parent) => () => {
	return new ECSA.Builder(scene)
		.withTag("map")
		.asGraphics()
		.localPos(0, 0)
		.anchor(0)
		.withAttribute("map", new Map())
		.withParent(parent)
}
