import * as ECSA from "../../libs/pixi-component"
import Dynamics from "../atributes/Dynamics"

/**
 * Component that updates position of an object
 */
export default class DynamicsComponent extends ECSA.Component {
	protected dynamics: Dynamics
	protected attrName: string

	constructor(attrName: string) {
		super()
		this.attrName = attrName
	}

	onInit() {
		this.dynamics = this.owner.getAttribute(this.attrName)
		if (this.dynamics == null) {
			// add an initial one
			this.dynamics = new Dynamics(0)
			this.owner.assignAttribute(this.attrName, this.dynamics)
		}
	}

	onUpdate(delta: number, absolute: number) {
		// calculate delta position
	}
}
