import * as PIXI from "pixi.js"
import * as ECSA from "../../libs/pixi-component"

export const map = (size: number) => {
	const graphics = new ECSA.Graphics()
	graphics
		.beginFill(0xf3f3ff)
		.lineStyle(20, 0xd8d8ff)
		.drawCircle(size, size, size + 10)
		.endFill()
	return graphics
}

export const sizeCell = (size: number) => {
	const center = new ECSA.Vector(size)

	const body = new ECSA.Graphics()
	body.beginFill(0x000000, 0.1)
		.drawCircle(center.x, center.y, size)
		.endFill()
		.beginFill(0x000000)
		.drawCircle(center.x, center.y, 4)
		.endFill()
	// body.blendMode = PIXI.BLEND_MODES.SUBTRACT

	return body
}

export const speedCell = (cellSize: number, size: number) => {
	const center = new ECSA.Vector(cellSize, cellSize * 2)
	const offsetSize = size + 4

	const body = new ECSA.Graphics()
	body.beginFill(0x0000ff, 0.3)
		.drawCircle(center.x, center.y - offsetSize, offsetSize)
		.endFill()

	return body
}

export const agilityCell = (
	cellSize: number,
	speedSize: number,
	size: number
) => {
	const center = new ECSA.Vector(cellSize, cellSize * 2)
	const offsetSize = speedSize + 4

	const body = new ECSA.Graphics()
	body.beginFill(0x00aaaa, 0.3)
		.drawCircle(center.x, center.y - offsetSize - size, size + offsetSize)
		.endFill()
		.beginHole()
		.drawCircle(center.x, center.y - offsetSize, offsetSize)
		.endHole()
		.beginHole()
		.moveTo(0, center.x)
		.arcTo(0, center.y * 2, center.x, center.y * 2, cellSize)
		.arcTo(center.x * 2, center.y * 2, center.x * 2, center.y, cellSize)
		.lineTo(center.x * 2, center.y * 2 + 8)
		.lineTo(0, center.y * 2 + 8)
		.lineTo(0, center.y)
		.closePath()
		.endHole()

	return body
}

export const strengthCell = (cellSize: number, size: number) => {
	const center = new ECSA.Vector(cellSize)
	const offset = 4

	const body = new ECSA.Graphics()
	body.beginFill(0xff0000, 0.5)
		.drawCircle(center.x, center.y, size + 2 + offset)
		.endFill()
		.beginHole()
		.drawCircle(center.x, center.y, offset)
		.endHole()

	return body
}

export const defenseCell = (cellSize: number, size: number) => {
	const center = new ECSA.Vector(cellSize)

	const body = new ECSA.Graphics()
	body.beginFill(0x000000, 0.2)
		.drawCircle(center.x, center.y, cellSize)
		.endFill()
		.beginHole()
		.drawCircle(center.x, center.y, cellSize - size + 1)
		.endHole()

	return body
}

export const sightCell = (cellSize: number, size: number) => {
	const center = new ECSA.Vector(cellSize, 0)
	const offsetSize = size + 4

	const body = new ECSA.Graphics()
	body.beginFill(0xffffff, 0.5)
		.drawCircle(center.x, center.y + offsetSize, offsetSize)
		.endFill()

	return body
}

export const mitochondrionCell = (
	position: ECSA.Vector,
	cellSize: number,
	size: number
) => {
	const center = new ECSA.Vector(cellSize)
	const offset = center.add(position)

	const body = new ECSA.Graphics()
	body.beginFill(0x00ff00, 0.3)
		.drawCircle(offset.x, offset.y, size)
		.endFill()

	return body
}

export const storageCell = (
	position: ECSA.Vector,
	cellSize: number,
	size: number
) => {
	const center = new ECSA.Vector(cellSize)
	const offset = center.add(position)

	const body = new ECSA.Graphics()
	body.beginFill(0xffff00, 0.3)
		.drawCircle(offset.x, offset.y, size)
		.endFill()

	return body
}

export const boostCell = (cellSize: number, size: number, speed: number) => {
	const center = new ECSA.Vector(cellSize, cellSize * 2)
	const maxSize = speed / 10
	const offsetSize = (size / 3) * maxSize
	const speedSize = speed + 4
	const actualSize = Math.max(1, offsetSize * speedSize * 0.75)

	const body = new ECSA.Graphics()
	body.beginFill(0x0044ff, 0.3)
		.drawCircle(center.x, center.y - speedSize, actualSize)
		.endFill()

	return body
}

export const poisonCell = (
	cellSize: number,
	intensity: number,
	maxIntensity: number
) => {
	const body = new ECSA.Graphics()
	body.beginFill(0xff8800, (intensity / maxIntensity) * 0.2)
		.drawCircle(cellSize, cellSize, cellSize)
		.endFill()

	return body
}

export const mutatorCell = (
	position: ECSA.Vector,
	cellSize: number,
	size: number
) => {
	const center = new ECSA.Vector(cellSize)
	const offset = center.add(position)

	const body = new ECSA.Graphics()
	body.beginFill(0x8800ff, 0.3)
		.drawCircle(offset.x, offset.y, size)
		.endFill()

	return body
}

export const sightBlocker = (
	width: number,
	height: number,
	cellSize: number,
	size: number
) => {
	const center = new ECSA.Vector(0)
	const offsetSize = cellSize + size

	const blocker = new ECSA.Container()
	const mask = new PIXI.Container()

	const body = new PIXI.Graphics()
	body.beginFill(0x000000)
		.drawRect(center.x, center.y, width, height)
		.endFill()

	const hole = new PIXI.Graphics()
	hole.beginFill(0xffffff)
		.drawCircle(width / 2, height / 2, offsetSize)
		.endFill()

	mask.addChild(body)
	mask.addChild(hole)
	mask.filters = [new PIXI.filters.AlphaFilter()]
	mask.filters[0].blendMode = PIXI.BLEND_MODES.MULTIPLY

	blocker.addChild(mask)

	blocker.zIndex = 10
	return blocker
}

export const food = (size: number = 1) => {
	const scaledSize = size * 4
	const body = new ECSA.Graphics()
	body.beginFill(0x00ff00, 0.25)
		.drawCircle(scaledSize, scaledSize, scaledSize)
		.endFill()

	return body
}

export const dna = (size: number = 1) => {
	const scaledSize = size + 8
	const body = new ECSA.Graphics()
	body.beginFill(0x8800ff, 0.25)
		.drawCircle(scaledSize, scaledSize, scaledSize)
		.endFill()

	return body
}
